const usersAPI = "http://localhost:5000/users";
const userImage = "https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png";

function createUserCard(userImage, userName, userEmail, userPhone, userCity) {
    const userCard = document.createElement("div");
    userCard.id = "userCard";

    const image = document.createElement("div");
    image.id = "userImage";
    image.style.backgroundImage = `url(${userImage})`

    const name = document.createElement("div");
    name.id = "userName";
    name.innerHTML = "Name: " + userName;

    userCard.appendChild(image);
    userCard.appendChild(name);

    document.getElementById("users").appendChild(userCard);
}

async function fetchData(url) {
    const data = await fetch(url)
        .then(response => { return response.json() })
        .catch(err => console.log(err));
    
    data.forEach(user => {
        createUserCard(
            userImage,
            user.name,
        );
    });
}

function main() {
    fetchData(usersAPI);
}

main();