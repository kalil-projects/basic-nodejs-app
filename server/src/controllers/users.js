const usersRepository = require('../repositories/users');

async function getAll(req, res) {
    const users = await usersRepository.getAll();

    return res.json(users);
}

async function getById(req, res) {
    const id = req.params.id;
    const user = await usersRepository.getById(id);

    return res.json(user);
}

module.exports = {
    getAll,
    getById
}