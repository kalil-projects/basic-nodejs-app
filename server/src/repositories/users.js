function getAll() {
    const users = [
        {
            id: 1,
            name: 'Kalil'
        },
        {
            id: 2,
            name: 'Luciana'
        },
        {
            id: 3,
            name: 'Pintita'
        },
        {
            id: 4,
            name: 'Sasha'
        }
    ]

    return users;
}

async function getById(id) {
    const users = getAll()

    const user = users.filter(x => x.id == id);

    return user;
}

module.exports = {
    getAll,
    getById
}