const { Router } = require('express');
const router = new Router();
const usersController = require("../controllers/users");

router.get('/', usersController.getAll);
router.get('/:id', usersController.getById);

module.exports = router;